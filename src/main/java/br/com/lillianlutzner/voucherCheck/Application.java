package br.com.lillianlutzner.voucherCheck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.lillianlutzner.voucherCheck.domain.Supermercado;
import br.com.lillianlutzner.voucherCheck.processor.VoucherCheckerProcessor;


@SpringBootApplication
public class Application {
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		Supermercado supermercado = new Supermercado();
	       
		VoucherCheckerProcessor validador = new VoucherCheckerProcessor(supermercado);
	        validador.validarVouchers();
		 
		
	
	}

}
