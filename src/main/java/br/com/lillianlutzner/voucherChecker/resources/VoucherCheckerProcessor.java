package br.com.lillianlutzner.voucherChecker.resources;

import java.text.DecimalFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.lillianlutzner.voucherChecker.domain.Supermercado;
import br.com.lillianlutzner.voucherChecker.domain.Voucher;
import jakarta.annotation.PostConstruct;


@RestController
@RequestMapping(value="/api/vouchers")
public class VoucherCheckerProcessor {
	
	private static final Logger LOGGER = LogManager.getLogger(VoucherCheckerProcessor.class);

	
	@Autowired
	private Supermercado supermercado;
	

	@PostConstruct
	public void init() {
	    for (int i = 1; i <= 1000000; i++) {
	        Voucher voucher = new Voucher();
	        supermercado.getVouchers().put(i, voucher);        
	    }
	    //validarVouchers();
	}
	
	//aqui termina a parte que estava na classe Supermercado


	@GetMapping("/")
	//private void validarVouchers() {
	public Map<String, Object> validarVouchers() {
		Map<String, Object> response = new HashMap<>();

		LocalDateTime horaInicial = LocalDateTime.now();
		String horaInicialConvert = horaInicial.format(DateTimeFormatter.ofPattern("dd-MM-yyyy::HH:mm:ss"));
		LOGGER.info(">>> Hora inicial do processamento: " + horaInicialConvert);
		LOGGER.info(">>> Iniciando validação...");
			

		Scanner scanner = new Scanner(System.in);
		String resposta;
		
		double doubleRandom = Math.random() * 100;
		double valorFinal = doubleRandom; //novo
		
		LOGGER.info("Valor total da compra: " + formatDouble(valorFinal));
		response.put("valorTotal", formatDouble(valorFinal));
				
		
		while (true) {
		    LOGGER.info(">>> Informe o número do voucher: ");

		    if (!scanner.hasNextInt()) {
		        LOGGER.error(">>> Entrada incorreta! Informe um número válido!");
		        scanner.nextLine();
		        continue;
		    }

		    int numeroVoucher = scanner.nextInt();
		    scanner.nextLine();

		    Voucher voucher = supermercado.getVouchers().get(numeroVoucher);

		    if (voucher == null) {
		        LOGGER.error(">>> Voucher não encontrado. Verifique o número digitado.");
		    } else if (voucher.isValido() && !voucher.isUtilizado()) {
		        voucher.setUtilizado(true);
		        response.put("numeroVoucher", numeroVoucher);
		        double desconto = descontoAleatorio();
		        LOGGER.info(">>> Voucher válido! Desconto aplicado: " + formatDouble(desconto) + "%");
		        response.put("desconto", formatDouble(desconto) + "%");
		        
		        double descontoTotal = (100 - desconto) / 100;
		        valorFinal = valorFinal * descontoTotal;
		        LOGGER.info(">>> O valor final da compra é de: R$" + formatDouble(valorFinal));
		        
		        if (valorFinal <= 0.02) {
			        LOGGER.info(">>> Não é necessário informar mais vouchers.");
			        break; 
			    }
		        
		    } else {
		        LOGGER.info(">>> Voucher inválido! Já utilizado ou não é mais válido.");
		    }
		    
		    
		    while (true) {
		        LOGGER.info(">>> Deseja informar mais um voucher? (S/N): ");
		        resposta = scanner.next().trim();

		        if (resposta.equalsIgnoreCase("s") || resposta.equalsIgnoreCase("n")) {
		            break; // Sai do loop interno
		        } else { 
		            LOGGER.error(">>> Informação incorreta! Informe S ou N.");
		        }
		    }

		    if (resposta.equalsIgnoreCase("n")) {
		        break; // Sai do loop externo
		    }
		}
		

		LOGGER.info(">>> Validação finalizada!");

		LocalDateTime horaFinal = LocalDateTime.now();
		String horaFinalConvert = horaFinal.format(DateTimeFormatter.ofPattern("dd-MM-yyyy::HH:mm:ss"));
		LOGGER.info(">>> Hora final do processamento: " + horaFinalConvert);

		Duration duracao = Duration.between(horaInicial, horaFinal);
		long horas = duracao.toHours();
		long minutos = duracao.toMinutesPart();
		long segundos = duracao.toSecondsPart();
		long miliseg = duracao.toMillis();

		LOGGER.info(">>> Tempo total de processamento: " + horas + ":" + minutos + ":" + segundos + ":" + miliseg);

        
        response.put("valorFinal", formatDouble(valorFinal));
        
        return response;
	
	}
	
	public static String formatDouble(double value) {
		
		DecimalFormat decimalFormat = new DecimalFormat("0.00");
		return decimalFormat.format(value);
		
	}
	
	public double descontoAleatorio() {
		return (Math.random() * 95) + 5;
	}

}
