package br.com.lillianlutzner.voucherChecker.domain;


public class Voucher {
	private boolean valido;
    private boolean utilizado;

    public Voucher() {
        this.valido = true;
        this.utilizado = false;
    }

    public boolean isValido() {
        return valido;
    }

    public void setValido(boolean valido) {
        this.valido = valido;
    }

    public boolean isUtilizado() {
        return utilizado;
    }

    public void setUtilizado(boolean utilizado) {
        this.utilizado = utilizado;
    }

}
