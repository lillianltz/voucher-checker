package br.com.lillianlutzner.voucherChecker.domain;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class Supermercado {
	
	private Map<Integer, Voucher> vouchers;

    public Supermercado() {
        this.vouchers = new HashMap<>();
    }

    public Map<Integer, Voucher> getVouchers() {
        return vouchers;
    }

}
